/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FrameTransmitterFactory.h"

#include "FrameTransmitter.h"
#include "backend/null/NullTransmitter.h"
#include "backend/rpi-gpio/RPiGPIOTransmitter.h"
#include "backend/esp8266-rts/ESP8266RTSFrameTransmitter.h"
#include "backend/debug/DebugTransmitter.h"

#ifdef RTS_HAVE_GPIOD
	#include "backend/gpiod/GPIODTransmitter.h"
#endif

namespace rts
{

std::shared_ptr<IFrameTransmitter> FrameTransmitterFactory::create(const DummyTransmitterConfiguration &,
		std::function<void(const std::string &)> debugLogger)
{
	return std::make_shared<FrameTransmitter<NullTransmitter>>(std::make_unique<NullTransmitter>(),
			std::move(debugLogger));
}

std::shared_ptr<IFrameTransmitter> FrameTransmitterFactory::create(const RPiGPIOTransmitterConfiguration & configuration,
		std::function<void(const std::string &)> debugLogger)
{
	return std::make_shared<FrameTransmitter<RPiGPIOTransmitter>>(std::make_unique<RPiGPIOTransmitter>(configuration.gpioNr),
			std::move(debugLogger));
}

#ifdef RTS_HAVE_GPIOD
std::shared_ptr<IFrameTransmitter> FrameTransmitterFactory::create(const GPIODTransmitterConfiguration & configuration,
		std::function<void(const std::string &)> debugLogger)
{
	return std::make_shared<FrameTransmitter<GPIODTransmitter>>(std::make_unique<GPIODTransmitter>(configuration.chipPath,
			configuration.offset, configuration.openDrain, configuration.activeLow), std::move(debugLogger));
}
#endif

std::shared_ptr<IFrameTransmitter> FrameTransmitterFactory::create(const ESP8266RTSTransmitterConfiguration & configuration,
		std::function<void(const std::string &)> debugLogger)
{
	return std::make_shared<ESP8266RTSFrameTransmitter>(configuration.hostname, std::move(debugLogger));
}

std::shared_ptr<IFrameTransmitter> FrameTransmitterFactory::create(const DebugTransmitterConfiguration & configuration,
		std::function<void(const std::string &)> debugLogger)
{
	return std::make_shared<FrameTransmitter<DebugTransmitter>>(std::make_unique<DebugTransmitter>(configuration.durationLogger),
			std::move(debugLogger));
}
}
