/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SomfyFrameHeader.h"
#include "Clock.h"

#include <chrono>
#include <array>

namespace rts
{

using namespace std::literals;

namespace
{
	constexpr Duration PULSES_SOMFY_HEADER_TELIS_NORMAL[] =
	{
		// wakeup
		Duration(10400us, true),
		Duration(7100us, false),

		// HW sync (1)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (2)
		Duration(2470us, true),
		Duration(2550us, false),

		// SW sync
		Duration(4800us, true),
		Duration(645us, false)
	};

	const Duration PULSES_SOMFY_HEADER_TELIS_REPEAT[] =
	{
		// HW sync (1)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (2)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (3)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (4)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (5)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (6)
		Duration(2470us, true),
		Duration(2550us, false),

		// HW sync (7)
		Duration(2470us, true),
		Duration(2550us, false),

		// SW sync
		Duration(4800us, true),
		Duration(645us, false)
	};

	constexpr Duration PULSES_SOMFY_HEADER_KEYTIS_NORMAL[] =
	{
		//pulse 1
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 2
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 3
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 4
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 5
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 6
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 7
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 8
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 9
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 10
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 11
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 12
		Duration(2500us, true),
		Duration(2600us, false),

		// SW sync
		Duration(4800us, true),
		Duration(645us, false)
	};

	constexpr Duration PULSES_SOMFY_HEADER_KEYTIS_REPEAT[] =
	{
		//pulse 1
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 2
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 3
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 4
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 5
		Duration(2500us, true),
		Duration(2600us, false),

		//pulse 6
		Duration(2500us, true),
		Duration(2600us, false),

		// SW sync
		Duration(4800us, true),
		Duration(645us, false)
	};
}

const SomfyFrameHeader SOMFY_HEADER_TELIS_NORMAL =
{
	PULSES_SOMFY_HEADER_TELIS_NORMAL,
	std::size(PULSES_SOMFY_HEADER_TELIS_NORMAL)
};

const SomfyFrameHeader SOMFY_HEADER_TELIS_REPEAT =
{
	PULSES_SOMFY_HEADER_TELIS_REPEAT,
	std::size(PULSES_SOMFY_HEADER_TELIS_REPEAT)
};

const SomfyFrameHeader SOMFY_HEADER_KEYTIS_NORMAL =
{
	PULSES_SOMFY_HEADER_KEYTIS_NORMAL,
	std::size(PULSES_SOMFY_HEADER_KEYTIS_NORMAL)
};

const SomfyFrameHeader SOMFY_HEADER_KEYTIS_REPEAT =
{
	PULSES_SOMFY_HEADER_KEYTIS_REPEAT,
	std::size(PULSES_SOMFY_HEADER_KEYTIS_REPEAT)
};

} // namespace rts
