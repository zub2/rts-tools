/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_FRAME_TRANSMITTER_H
#define RTS_FRAME_TRANSMITTER_H

#include <memory>

#include "FrameTransmitterBase.h"
#include "IFrameTransmitter.h"

namespace rts
{

template<typename TTransmitter>
class FrameTransmitter: public FrameTransmitterBase
{
public:
	FrameTransmitter(std::unique_ptr<TTransmitter> && transmitter, std::function<void(const std::string &)> debugLogger):
				FrameTransmitterBase(std::move(debugLogger)),
				m_transmitter(std::move(transmitter))
	{}

protected:
	virtual void sendBuffer(const std::vector<Duration> & buffer) override
	{
		m_transmitter->transmit(buffer);
	}

private:
	std::unique_ptr<TTransmitter> m_transmitter;
};

}

#endif // RTS_FRAME_TRANSMITTER_H
