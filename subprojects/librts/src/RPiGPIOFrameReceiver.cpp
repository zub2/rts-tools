/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RPiGPIOFrameReceiver.h"

#include "DurationTracker.h"
#include "SomfyDecoder.h"

namespace rts
{

RPiGPIOFrameReceiver::RPiGPIOFrameReceiver(unsigned gpioNr, size_t bufferSize,
		const Clock::duration & samplePeriod, double tolerance):
	m_gpioNr(gpioNr),
	m_bufferSize(bufferSize),
	m_samplePeriod(samplePeriod),
	m_tolerance(tolerance)
{}

void RPiGPIOFrameReceiver::run(IFrameListener & frameListener)
{
	std::unique_lock lock(m_stopMutex);

	// if stop was called before this is reached, then just don't start
	if (m_stopped)
		return;

	m_recordingThread = std::make_unique<RecordingThread>(m_gpioNr, m_bufferSize, m_samplePeriod);

	DurationTracker<RecordingThread> durationTracker(*m_recordingThread);
	SomfyDecoder<DurationTracker<RecordingThread>> decoder(durationTracker, m_tolerance, frameListener);

	m_recordingThread->start();

	// let stop() proceed
	lock.unlock();

	decoder.run();
}

void RPiGPIOFrameReceiver::stop()
{
	std::scoped_lock lock(m_stopMutex);
	if (!m_stopped)
	{
		m_stopped = true;
		if (m_recordingThread)
			m_recordingThread->stop();
	}
}

}
