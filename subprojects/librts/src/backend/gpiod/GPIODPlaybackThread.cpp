/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <backend/gpiod/GPIODPlaybackThread.h>

namespace rts
{

/*
 * Minimize the overhead gpiodcxx's line_request::set_value() creates and
 * allocate the offset and value vectors now - and keep reusing those.
 * Calling set_value() directly means 2 1-element vectors get created every
 * time. Nein, danke!
 */
GPIODPlaybackThread::GPIODPlaybackThread(gpiod::line_request & lineRequest):
	m_lineRequest(lineRequest),
	m_offsets{0},
	m_values{gpiod::line::value::INACTIVE}
{
}

void GPIODPlaybackThread::write(bool value)
{
	m_values.front() = value ? gpiod::line::value::ACTIVE : gpiod::line::value::INACTIVE;
	m_lineRequest.set_values(m_offsets, m_values);
}

} // namespace rts
