/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "backend/gpiod/GPIODTransmitter.h"

namespace rts
{

// https://archive.fosdem.org/2018/schedule/event/new_gpio_interface_for_linux/

GPIODTransmitter::GPIODTransmitter(const std::filesystem::path& chipPath, unsigned offset, bool openDrain, bool activeLow):
	m_offset(offset),
	m_lineRequest(allocateLine(chipPath, offset, openDrain, activeLow)),
	m_playbackThread(m_lineRequest)
{
	m_playbackThread.start();
}

void GPIODTransmitter::transmit(const std::vector<Duration> & samples)
{
	for (const Duration & sample : samples)
	{
		const auto newValue = sample.second ? gpiod::line::value::ACTIVE : gpiod::line::value::INACTIVE;
		m_lineRequest.set_value(m_offset, newValue);
		std::this_thread::sleep_for(sample.first);
	}
}

GPIODTransmitter::~GPIODTransmitter()
{
	m_playbackThread.stop();
}

gpiod::line_request GPIODTransmitter::allocateLine(const std::filesystem::path& chipPath, unsigned offset,
		bool openDrain, bool activeLow)
{
	auto lineSettings = gpiod::line_settings()
		.set_direction(gpiod::line::direction::OUTPUT)
		.set_drive(openDrain ? gpiod::line::drive::OPEN_DRAIN: gpiod::line::drive::PUSH_PULL)
		.set_active_low(activeLow);

	return gpiod::chip(chipPath).prepare_request()
			.set_consumer("librts")
			.add_line_settings(offset, std::move(lineSettings))
			.do_request();
}

}
