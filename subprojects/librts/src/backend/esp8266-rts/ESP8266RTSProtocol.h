/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_ESP8266_RTS_PROTOCOL_H
#define RTS_ESP8266_RTS_PROTOCOL_H

#include <cstdint>
#include <vector>

#include "ByteBuffer.h"
#include "SomfyFrame.h"

namespace rts
{
	enum class Command: uint8_t
	{
		Ping,
		RTSCommand,
		RTSCommandContinuous
	};

	constexpr uint32_t makeUInt32LE(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3)
	{
		return (b3 << 24) | (b2 << 16) | (b1 << 8) | b0;
	}

	extern const char * PORT;
	constexpr uint32_t MAGIC = makeUInt32LE('!', 'R', 'T', 'S');
	constexpr uint8_t VERSION = 1;

	std::vector<uint8_t> makeRTSCommand(const SomfyTelisFrame & frame);
}

#endif // RTS_ESP8266_RTS_PROTOCOL_H
