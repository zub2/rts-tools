/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_BYTE_BUFFER_H
#define RTS_BYTE_BUFFER_H

#include <vector>
#include <cstdint>

namespace rts
{
	class ByteBuffer
	{
	public:
		ByteBuffer & appendUInt8(uint8_t v)
		{
			m_buffer.push_back(v);
			return *this;
		}

		ByteBuffer & appendUInt16LE(uint16_t v)
		{
			m_buffer.push_back(static_cast<uint8_t>((v >> 0*8) & 0xff));
			m_buffer.push_back(static_cast<uint8_t>((v >> 1*8) & 0xff));
			return *this;
		}

		ByteBuffer & appendUInt32LE(uint32_t v)
		{
			m_buffer.push_back(static_cast<uint8_t>((v >> 0*8) & 0xff));
			m_buffer.push_back(static_cast<uint8_t>((v >> 1*8) & 0xff));
			m_buffer.push_back(static_cast<uint8_t>((v >> 2*8) & 0xff));
			m_buffer.push_back(static_cast<uint8_t>((v >> 3*8) & 0xff));
			return *this;
		}

		std::vector<uint8_t> && getContent()
		{
			return std::move(m_buffer);
		}

	private:
		std::vector<uint8_t> m_buffer;
	};
}

#endif // RTS_BYTE_BUFFER_H
