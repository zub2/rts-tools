/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_GET_ADDR_INFO_ERR_H
#define RTS_GET_ADDR_INFO_ERR_H

#include <system_error>
#include <string>

struct GetAddrInfoErrCategory : std::error_category
{
	virtual const char * name() const noexcept override;
	virtual std::string message(int ev) const override;
};

extern const GetAddrInfoErrCategory gaiErrCategory;

struct GAIErrc
{
	explicit GAIErrc(int value):
		m_value(value)
	{}

	explicit operator int()
	{
		return m_value;
	}

private:
	int m_value;
};

inline std::error_code make_error_code(GAIErrc e)
{
	return {static_cast<int>(e), gaiErrCategory};
}

#endif // RTS_GET_ADDR_INFO_ERR_H
