/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SomfyFrameMatcher.h"
#include "SomfyFrameHeader.h"

#include <cmath>
#include <chrono>

#include <iostream>

// useful for debugging, might make sense to turn this into a runtime option?
//#define VERBOSE_MATCHER

namespace rts
{

SomfyFrameMatcher::SomfyFrameMatcher(double tolerance):
	m_tolerance(tolerance)
{
	m_matchers.reserve(4);
	m_matchers.emplace_back(MatcherEntry{SOMFY_HEADER_TELIS_NORMAL, SomfyRemoteType::telis, SomfyFrameType::normal});
	m_matchers.emplace_back(MatcherEntry{SOMFY_HEADER_TELIS_REPEAT, SomfyRemoteType::telis, SomfyFrameType::repeat});
	m_matchers.emplace_back(MatcherEntry{SOMFY_HEADER_KEYTIS_NORMAL, SomfyRemoteType::keytis, SomfyFrameType::normal});
	m_matchers.emplace_back(MatcherEntry{SOMFY_HEADER_KEYTIS_REPEAT, SomfyRemoteType::keytis, SomfyFrameType::repeat});
}

void SomfyFrameMatcher::reset()
{
	for (auto & matcherEntry : m_matchers)
	{
		matcherEntry.matcher.reset();
	}
}

std::optional<SomfyFrameMatcher::FrameMatch> SomfyFrameMatcher::newTransition(const Duration & duration)
{
	std::optional<FrameMatch> frameMatch;
	std::optional<Clock::duration> remainingDuration;
	for (auto & matcherEntry : m_matchers)
	{
		remainingDuration = matcherEntry.matcher.newTransition(duration, m_tolerance);
		if (remainingDuration)
		{
			// frame was matched
			frameMatch = FrameMatch(matcherEntry.remoteType, matcherEntry.frameType, *remainingDuration, duration.second);
			break;
		}
	}

	if (frameMatch)
	{
		reset();
	}

	return frameMatch;
}

SomfyFrameMatcher::SequenceMatcher::SequenceMatcher(const SomfyFrameHeader & header):
	m_sequence(header.durations),
	m_sequenceSize(header.count),
	m_matchedCount(0)
{}

std::optional<Clock::duration> SomfyFrameMatcher::SequenceMatcher::newTransition(const Duration & duration, double tolerance)
{
	if (m_matchedCount == m_sequenceSize)
		throw std::runtime_error("SequenceMatcher::newTransition() called too many times w/o a reset!");

	const bool newState = duration.second;
	const bool expectedNewState = m_sequence[m_matchedCount].second;

#ifdef VERBOSE_MATCHER
	std::cout << "matcher: expected duration " <<
		std::chrono::duration_cast<std::chrono::microseconds>(m_sequence[m_matchedCount].first).count() << "µs, "
		"actual: " << std::chrono::duration_cast<std::chrono::microseconds>(duration.first).count() << "µs" << std::endl;
#endif

	if (newState == expectedNewState &&
			((matchDuration(duration.first, m_sequence[m_matchedCount].first, tolerance) ||
			(m_matchedCount + 1 == m_sequenceSize && matchDurationAtLeast(duration.first, m_sequence[m_matchedCount].first, tolerance))))
		)
	{
		m_matchedCount++;
#ifdef VERBOSE_MATCHER
		std::cout << "matcher: matched: " << m_matchedCount << std::endl;
#endif

		// TODO: clean this up
		if (m_matchedCount == m_sequenceSize)
		{
			if (matchDuration(duration.first, m_sequence[m_matchedCount-1].first, tolerance))
				return Clock::duration::zero();
			else
				return std::max(duration.first - m_sequence[m_matchedCount-1].first, Clock::duration::zero());
		}
		return std::nullopt;
	}
	else
	{
#ifdef VERBOSE_MATCHER
		std::cout << "matcher: resetting match" << std::endl;
#endif
		reset();
		return std::nullopt;
	}
}

bool SomfyFrameMatcher::SequenceMatcher::matchDuration(Clock::duration actual, Clock::duration expected, double tolerance)
{
	const double a = std::chrono::duration<double>(actual).count();
	const double e = std::chrono::duration<double>(expected).count();

	return fabs(a - e) < tolerance * e;
}

bool SomfyFrameMatcher::SequenceMatcher::matchDurationAtLeast(Clock::duration actual, Clock::duration expected, double tolerance)
{
	const double a = std::chrono::duration<double>(actual).count();
	const double e = std::chrono::duration<double>(expected).count();

	return a - e > - tolerance * e;
}

void SomfyFrameMatcher::SequenceMatcher::reset()
{
	m_matchedCount = 0;
}

} // namespace rts
