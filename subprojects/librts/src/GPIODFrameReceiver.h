/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_GPIOD_FRAME_RECEIVER_H
#define RTS_GPIOD_FRAME_RECEIVER_H

#include <memory>
#include <mutex>

#include "IFrameReceiver.h"
#include "backend/gpiod/GPIODReceiver.h"

namespace rts
{

class GPIODFrameReceiver: public IFrameReceiver
{
public:
	GPIODFrameReceiver(const std::string & gpioChip, unsigned offset, bool activeLow, size_t bufferSize, double tolerance);

	virtual void run(IFrameListener & frameListener) override;
	virtual void stop() override;

private:
	std::mutex m_stopMutex;
	bool m_stopped = false;

	std::string m_gpioChip;
	unsigned m_offset;
	bool m_activeLow;
	size_t m_bufferSize;
	double m_tolerance;

	std::unique_ptr<GPIODReceiver> m_gpiodReceiver;
};

}

#endif // RTS_GPIOD_FRAME_RECEIVER_H
