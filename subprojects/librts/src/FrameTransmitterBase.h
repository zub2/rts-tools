/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_FRAME_TRANSMITTER_BASE_H
#define RTS_FRAME_TRANSMITTER_BASE_H

#include <vector>
#include <functional>
#include <string>

#include "IFrameTransmitter.h"
#include "Duration.h"
#include "DurationBuffer.h"
#include "SomfyFrame.h"
#include "SomfyFrameType.h"

namespace rts
{

class FrameTransmitterBase: public IFrameTransmitter
{
public:
	FrameTransmitterBase(std::function<void(const std::string &)> && debugLogger);

	virtual void send(const SomfyTelisFrame & frame, size_t repeatFrameCount) override;

protected:
	virtual void sendBuffer(const std::vector<Duration> & buffer) = 0;

private:
	void appendFrame(DurationBuffer & buffer, SomfyFrameType frameType, const std::vector<Duration> & frameSamples);
	std::vector<Duration> getEncodedFramePayload(const SomfyTelisFrame & frame);

	std::function<void(const std::string &)> m_debugLogger;

	static const rts::Duration INTER_FRAME_GAP;
};

}

#endif // RTS_FRAME_TRANSMITTER_BASE_H
