/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SomfyFrame.h"

#include <stdexcept>

namespace rts
{

// this is based on https://pushstack.wordpress.com/somfy-rts-protocol/
SomfyFrameBase::SomfyFrameBase():
	SomfyFrameBase(0, Action::my, 0, 0)
{}

SomfyFrameBase::SomfyFrameBase(uint8_t key, Action ctrl, uint16_t rollingCode, uint32_t address):
	m_key(key),
	m_ctrl(ctrl),
	m_rollingCode(rollingCode),
	m_address(address)
{
	// address is encoded in 3 bytes, so it can only hold values 0x000000 .. 0xffffff
	if (address > UINT32_C(0xffffff))
		throw std::runtime_error("Address must be lower than 0x1000000.");
}

void SomfyFrameBase::deobfuscate(std::vector<uint8_t> & bytes)
{
	for (size_t i = bytes.size() - 1; i > 0; i--)
		bytes[i] ^= bytes[i-1];
}

void SomfyFrameBase::obfuscate(std::vector<uint8_t> & bytes)
{
	for (size_t i = 1; i < bytes.size(); i++)
		bytes[i] ^= bytes[i-1];
}

uint8_t SomfyFrameBase::checksum(std::vector<uint8_t>::const_iterator begin, std::vector<uint8_t>::const_iterator end)
{
	uint8_t c = 0;
	for (auto it = begin; it != end; ++it)
		c = c ^ *it ^ (*it >> 4);

	return c & 0xf;
}

void SomfyFrameBase::decodeCommonBase(const std::vector<uint8_t> & deobfuscatedBytes)
{
	if (deobfuscatedBytes.size() < 7)
		throw std::runtime_error("decodeCommonBase: input buffer too small");

	m_key = deobfuscatedBytes[0];
	m_ctrl = static_cast<Action>(deobfuscatedBytes[1] >> 4);
	m_rollingCode = (static_cast<uint16_t>(deobfuscatedBytes[2]) << 8) | deobfuscatedBytes[3];
	m_address = (static_cast<uint32_t>(deobfuscatedBytes[4]) << 16) | (static_cast<uint32_t>(deobfuscatedBytes[5]) << 8)
		| deobfuscatedBytes[6];
}

SomfyTelisFrame SomfyTelisFrame::fromBytes(std::vector<uint8_t> bytes)
{
	if (bytes.size() != FRAME_SIZE)
		throw std::runtime_error("invalid frame size");

	deobfuscate(bytes);

	SomfyTelisFrame frame;
	frame.decodeCommonBase(bytes);

	const uint8_t c = checksum(bytes.begin(), bytes.end());
	if (c != 0)
		throw WrongFrameChecksumException("invalid Telis checksum!");

	return frame;
}

std::vector<uint8_t> SomfyTelisFrame::getBytes() const
{
	std::vector<uint8_t> frame;
	frame.resize(FRAME_SIZE);

	frame[0] = m_key;
	frame[1] = static_cast<uint8_t>(m_ctrl) << 4;
	frame[2] = static_cast<uint8_t>(m_rollingCode >> CHAR_BIT);
	frame[3] = static_cast<uint8_t>(m_rollingCode);
	frame[4] = static_cast<uint8_t>(m_address >> 2*CHAR_BIT);
	frame[5] = static_cast<uint8_t>(m_address >> 1*CHAR_BIT);
	frame[6] = static_cast<uint8_t>(m_address >> 0*CHAR_BIT);

	frame[1] |= checksum(frame.begin(), frame.end());
	obfuscate(frame);
	return frame;
}

SomfyKeytisFrame SomfyKeytisFrame::fromBytes(std::vector<uint8_t> bytes)
{
	if (bytes.size() != FRAME_SIZE)
		throw std::runtime_error("invalid frame size");

	SomfyKeytisFrame frame;

	// copy the extra bytes
	std::copy(bytes.begin() + SomfyTelisFrame::FRAME_SIZE, bytes.end(), frame.m_extraBytes.begin());

	bytes.resize(SomfyTelisFrame::FRAME_SIZE);
	deobfuscate(bytes);
	frame.decodeCommonBase(bytes);

	// the checksum seems to only cover the common part - but the extra bytes
	// were trimmed above
	const uint8_t c = checksum(bytes.begin(), bytes.end());
	if (c != 0)
		throw WrongFrameChecksumException("invalid Keytis checksum!");

	return frame;
}

const std::array<uint8_t, SomfyKeytisFrame::EXTRA_BYTES> & SomfyKeytisFrame::getExtraBytes() const
{
	return m_extraBytes;
}

} // namespace rts
