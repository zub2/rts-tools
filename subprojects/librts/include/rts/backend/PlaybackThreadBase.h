/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_PLAYBACK_THREAD_BASE_H
#define RTS_PLAYBACK_THREAD_BASE_H

#include <utility>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <chrono>
#include <stdexcept>

#include "../Clock.h"
#include "../Duration.h"
#include "../ThreadPrio.h"

namespace rts
{

template<typename TDerived>
class PlaybackThreadBase
{
public:
	PlaybackThreadBase():
		m_running(false),
		m_playbackFinished(false)
	{}

	void start()
	{
		std::lock_guard<std::mutex> g(m_mutex);

		if (m_running)
			throw std::runtime_error("playback thread already started");

		m_running = true;
		m_thread = std::thread(&PlaybackThreadBase::playbackLoop, this);
		setThreadSchedulerAndPrio(m_thread, SCHED_FIFO);
	}

	void stop()
	{
		bool join = false;

		{
			std::lock_guard<std::mutex> g(m_mutex);
			if (m_running)
			{
				m_running = false;
				m_playbackRequestCondVar.notify_all();
				join = true;
			}
		}

		if (join)
			m_thread.join();
	}

	void play(const std::vector<Duration> & samples)
	{
		std::unique_lock<std::mutex> g(m_mutex);

		if (!m_running)
			throw std::runtime_error("PlaybackThreadBase not running!");

		m_playbackBuffer = samples;
		m_playbackFinished = false;
		m_playbackRequestCondVar.notify_one();

		while (!m_playbackFinished)
		{
			m_playbackFinishedCondVar.wait(g);
		}
	}

private:
	void playbackLoop()
	{
		while (true)
		{
			std::unique_lock<std::mutex> g(m_mutex);
			while (m_playbackBuffer.empty() && m_running)
				m_playbackRequestCondVar.wait(g);

			if (!m_running)
				return;

			// play back...
			for (const Duration & s : m_playbackBuffer)
			{
				static_cast<TDerived*>(this)->write(s.second);
				std::this_thread::sleep_for(s.first);
			}

			m_playbackBuffer.clear();
			m_playbackFinished = true;
			m_playbackFinishedCondVar.notify_all();
		}
	}

	std::mutex m_mutex;
	std::thread m_thread;
	bool m_running;
	bool m_playbackFinished;
	std::condition_variable m_playbackFinishedCondVar;

	std::vector<Duration> m_playbackBuffer;
	std::condition_variable m_playbackRequestCondVar;
};

} // namespace rts

#endif // RTS_PLAYBACK_THREAD_BASE_H
