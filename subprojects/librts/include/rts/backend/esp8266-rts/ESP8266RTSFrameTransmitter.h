/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_ESP8266_RTS_FRAME_TRANSMITTER_H
#define RTS_ESP8266_RTS_FRAME_TRANSMITTER_H

#include <string>
#include <functional>

#include "../../IFrameTransmitter.h"
#include "../../SomfyFrame.h"

namespace rts
{

class ESP8266RTSFrameTransmitter: public IFrameTransmitter
{
public:
	ESP8266RTSFrameTransmitter(std::string host, std::function<void(const std::string &)> debugLogger);

	virtual void send(const SomfyTelisFrame & frame, size_t repeatFrameCount) override;

private:
	std::string m_host;
	std::function<void(const std::string &)> m_debugLogger;
};

}

#endif // RTS_ESP8266_RTS_FRAME_TRANSMITTER_H
