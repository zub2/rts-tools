/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FRAME_LISTENER_H
#define FRAME_LISTENER_H

#include <vector>
#include <string>
#include <array>
#include <cstdint>

#include "rts/IFrameListener.h"

class FrameListener: public rts::IFrameListener
{
public:
	virtual void onFrameDetected(rts::SomfyRemoteType remoteType, rts::SomfyFrameType frameType) override;
	virtual void onFrameDecoded(rts::SomfyFrameType frameType, const rts::SomfyTelisFrame & frame) override;
	virtual void onFrameDecoded(rts::SomfyFrameType frameType, const rts::SomfyKeytisFrame & frame) override;
	virtual void onFrameDecodeError(FrameError error, const std::vector<bool> & bits) override;

private:
	void logFrame(const rts::SomfyFrameBase & frame);

	template<std::size_t SIZE>
	void logByteArray(const std::string & description, const std::array<uint8_t, SIZE> & bytes);

	template<std::size_t SIZE>
	void logBits(const std::array<uint8_t, SIZE> & bytes);

	template<std::size_t SIZE>
	void logBytes(const std::array<uint8_t, SIZE> & bytes);

	std::string getButtonName(rts::SomfyTelisFrame::Action code);
	std::string stringifyBits(const std::vector<bool> & bits);
};

#endif // FRAME_LISTENER_H
