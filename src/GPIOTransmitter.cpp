/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <vector>

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>

#include "DurationFileReader.h"
#include "rts/DurationBuffer.h"
#include "rts/backend/rpi-gpio/RPiGPIOTransmitter.h"

#ifdef RTS_HAVE_GPIOD
	#include "rts/backend/gpiod/GPIODTransmitter.h"
#endif

#include "ParseTransmitOptions.h"

namespace po = boost::program_options;

namespace
{
	class GPIOTransmitter: public GPIOTransmitBackendParametersReceiver
	{
	public:
		virtual void onRPiGPIOBackendParams(unsigned gpioNr) override
		{
			m_runFn = [gpioNr](const std::vector<rts::Duration> & durations){
				rts::RPiGPIOTransmitter transmitter(gpioNr);
				transmitter.transmit(durations);
			};
		}

#ifdef RTS_HAVE_GPIOD
		virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned line, bool openDrain, bool activeLow) override
		{
			m_runFn = [chipPath, line, openDrain, activeLow](const std::vector<rts::Duration> & durations){
				rts::GPIODTransmitter transmitter(chipPath, line, openDrain, activeLow);
				transmitter.transmit(durations);
			};
		}
#endif

		void run(const std::string & inputFile)
		{
			m_runFn(readDurations(inputFile));
		}

	private:
		static std::vector<rts::Duration> readDurations(const std::string &inputFile)
		{
			DurationFileReader reader(inputFile);
			rts::DurationBuffer buffer;

			std::optional<rts::Duration> d = reader.get();
			while (d)
			{
				buffer << *d;
				d = reader.get();
			}

			return buffer.get();
		}

		std::function<void(const std::vector<rts::Duration> & durations)> m_runFn;
	};
}

int main(int argc, char * argv[])
{
	try
	{
		std::string inputFile;

		po::options_description toolOptions("Available options");
		toolOptions.add_options()
			("file,f", boost::program_options::value(&inputFile)->required(),
					"The GPIO log file to play.")
		;

		GPIOTransmitter gpioTransmitter;
		std::optional<po::variables_map> variablesMap = parseGPIOTransmitOptions(argc, argv, toolOptions, gpioTransmitter);
		if (!variablesMap)
			return 0; // help was requested

		gpioTransmitter.run(inputFile);
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	return 0;
}
