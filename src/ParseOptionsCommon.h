/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARSE_OPTIONS_COMMON_H
#define PARSE_OPTIONS_COMMON_H

#include <string>

// string constants shared by receive and transmit option parsing
extern const std::string GPIO_NR;

extern const std::string BACKEND_RPI_GPIO;
extern const std::string RPI_GPIO_DESCRIPTION_NAME;

extern const std::string BACKEND_GPIOD;
extern const std::string GPIOD_DESCRIPTION_NAME;

extern const std::string BACKEND_ESP8266_RTS;
extern const std::string ESP8266_RTS_DESCRIPTION_NAME;

extern const std::string BACKEND_DEBUG;
extern const std::string DEBUG_DESCRIPTION_NAME;

#ifdef RTS_HAVE_GPIOD
extern const std::string CHIP;
extern const std::string LINE;
extern const std::string ACTIVE_LOW;
#endif

#endif // PARSE_OPTIONS_COMMON_H
