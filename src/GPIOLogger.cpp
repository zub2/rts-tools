/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <chrono>
#include <sstream>
#include <cstdlib>
#include <optional>
#include <filesystem>

#include <boost/program_options.hpp>

#include "rts/Clock.h"
#include "rts/Transition.h"
#include "rts/backend/rpi-gpio/RecordingThread.h"

#ifdef RTS_HAVE_GPIOD
	#include "rts/backend/gpiod/GPIODReceiver.h"
#endif

#include "rts/DurationTracker.h"
#include "GPIOLogWriter.h"
#include "ParseReceiveOptions.h"

using std::chrono::steady_clock;
namespace po = boost::program_options;

namespace
{
	const std::string DEFAULT_FILENAME = "gpio_log.bin";
	constexpr unsigned DEFAULT_DURATION_S = 5;
	constexpr size_t DEFAULT_BUFFER_SIZE = 1000;

	template<typename TReceiver>
	void record(TReceiver & receiver, const rts::Clock::duration & recordingDuration,
			size_t bufferSize, const std::string & outputFileName)
	{
		GPIOLogWriter writer(outputFileName);

		std::cout << "Starting recording... " << std::flush;

		const steady_clock::time_point end = steady_clock::now() + recordingDuration;
		receiver.start();

		std::thread stopThread([&end, &receiver](){
			std::this_thread::sleep_until(end);
			receiver.stop();
		});

		rts::DurationTracker<TReceiver> durationTracker(receiver);
		std::optional<rts::Duration> duration = durationTracker.get();
		while (duration)
		{
			writer.write(*duration);
			duration = durationTracker.get();
		}

		stopThread.join();

		std::cout << "done" << std::endl;
	}

	class GPIOLogger: public GPIOReceiveBackendParametersReceiver
	{
	public:
		virtual void onRPiGPIOBackendParams(unsigned gpioNr, size_t bufferSize, const rts::Clock::duration & samplePeriod) override
		{
			m_recordingThread.emplace(gpioNr, bufferSize, samplePeriod);
		}

#ifdef RTS_HAVE_GPIOD
		virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned offset, bool activeLow, size_t bufferSize) override
		{
			m_gpiodReceiver.emplace(chipPath, offset, activeLow, bufferSize);
		}
#endif

		void run(const rts::Clock::duration & recordingDuration, size_t bufferSize,
				const std::string & outputFileName)
		{
			if (m_recordingThread)
				record(*m_recordingThread, recordingDuration, bufferSize, outputFileName);
#ifdef RTS_HAVE_GPIOD
			else if (m_gpiodReceiver)
				record(*m_gpiodReceiver, recordingDuration, bufferSize, outputFileName);
#endif
		}

	private:
		std::optional<rts::RecordingThread> m_recordingThread;
#ifdef RTS_HAVE_GPIOD
		std::optional<rts::GPIODReceiver> m_gpiodReceiver;
#endif
	};
}

int main(int argc, char * argv[])
{
	try
	{
		unsigned recordingDuration = DEFAULT_DURATION_S;
		size_t bufferSize = DEFAULT_BUFFER_SIZE;
		std::string outputFileName = DEFAULT_FILENAME;

		po::options_description toolOptions("Available options");
		toolOptions.add_options()
			("duration,d", boost::program_options::value(&recordingDuration),
				(std::string("Number of seconds to keep recording. Default: ") + std::to_string(recordingDuration)).c_str())
			("buffer-size,b", boost::program_options::value(&bufferSize),
				(std::string("Size of buffer (number of entries). Default: ") + std::to_string(bufferSize)).c_str())
			("file,f", boost::program_options::value(&outputFileName),
				(std::string("Name of the out file. Default: ") + outputFileName).c_str())
		;

		GPIOLogger gpioLogger;
		std::optional<po::variables_map> variablesMap = parseGPIOReceiveOptions(argc, argv, toolOptions, gpioLogger);
		if (!variablesMap)
			return 0; // help was requested

		gpioLogger.run(std::chrono::seconds(recordingDuration), bufferSize, outputFileName);
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	return 0;
}
