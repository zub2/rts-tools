/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ParseReceiveOptions.h"
#include "ParseOptionsCommon.h"

#include "ParseDependentOptions.h"

using namespace std::literals;
namespace po = boost::program_options;

namespace
{
	constexpr unsigned RPI_GPIO_DEFAULT_SAMPLE_PERIOD_US = 10;
	constexpr size_t RPI_GPIO_DEFAULT_BUFFER_SIZE = 1000;
	const std::string BUFFER_SIZE = "buffer-size";
	const std::string SAMPLE_PERIOD = "sample-period";

#ifdef RTS_HAVE_GPIOD
	constexpr size_t GPIOD_DEFAULT_BUFFER_SIZE = 1000;
#endif

	namespace frame_receiver
	{
		po::options_description getRPiGPIOBackendOptions()
		{
			po::options_description rpiGPIOBackendOptions(RPI_GPIO_DESCRIPTION_NAME);
			rpiGPIOBackendOptions.add_options()
				((GPIO_NR + ",n").c_str(), po::value<unsigned>()->required(),
					"The GPIO number to use.")
				((BUFFER_SIZE + ",b").c_str(), boost::program_options::value<size_t>(),
					("Size of buffer (number of entries). Default: "s + std::to_string(RPI_GPIO_DEFAULT_BUFFER_SIZE)).c_str())
				((SAMPLE_PERIOD + ",s").c_str(), boost::program_options::value<size_t>(),
					("Sample period in µs. Default: "s + std::to_string(RPI_GPIO_DEFAULT_SAMPLE_PERIOD_US)).c_str())
			;

			return rpiGPIOBackendOptions;
		}

		void storeRPiGPIOBackendOptions(ReceiveBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onRPiGPIOBackendParams(
					variablesMap[GPIO_NR].as<unsigned>(),
					valueOr(variablesMap, BUFFER_SIZE, RPI_GPIO_DEFAULT_BUFFER_SIZE),
					std::chrono::microseconds(valueOr(variablesMap, SAMPLE_PERIOD, RPI_GPIO_DEFAULT_SAMPLE_PERIOD_US))
			);
		}

#ifdef RTS_HAVE_GPIOD
		po::options_description getGPIODBackendOptions()
		{
			po::options_description gpiodBackendOptions(GPIOD_DESCRIPTION_NAME);
			gpiodBackendOptions.add_options()
				((CHIP + ",C").c_str(), po::value<std::filesystem::path>()->required(),
					"Path of the GPIO chip device, e.g. /dev/gpiochip0.")
				((LINE + ",L").c_str(), po::value<unsigned>()->required(),
					"GPIO line number.")
				((BUFFER_SIZE + ",b").c_str(), boost::program_options::value<size_t>(),
					("Size of buffer (number of entries). Default: "s + std::to_string(GPIOD_DEFAULT_BUFFER_SIZE)).c_str())
			;

			return gpiodBackendOptions;
		}

		void storeGPIODBackendOptions(ReceiveBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onGPIODBackendParams(
				variablesMap[CHIP].as<std::filesystem::path>(),
				variablesMap[LINE].as<unsigned>(),
				variablesMap.count(ACTIVE_LOW) != 0,
				valueOr(variablesMap, BUFFER_SIZE, GPIOD_DEFAULT_BUFFER_SIZE)
			);
		}
#endif

#ifdef RTS_HAVE_RTLSDR
		const std::string DEVICE_IDX = "device-idx";
		const std::string BUFFER_COUNT = "buffer-count";
		const std::string LOG_NAME = "log-name";
		const std::string GAIN = "gain";

		constexpr uint32_t RTLSDR_DEFAULT_DEVICE_IDX = 0;
		constexpr size_t RTLSDR_DEFAULT_BUFFER_SIZE = 256*1024; // bytes
		constexpr size_t RTLSDR_DEFAULT_BUFFER_COUNT = 5;

		const std::string GAIN_AUTO = "auto";

		struct RTLSDRGain
		{
			std::optional<int> value;
		};

		std::ostream & operator<<(std::ostream & os, const RTLSDRGain & gain)
		{
			if (gain.value)
				os << *gain.value;
			else
				os << GAIN_AUTO;

			return os;
		}

		std::istream & operator>>(std::istream & in, RTLSDRGain & gain)
		{
			std::string s;
			in >> s;
			if (s == GAIN_AUTO)
				gain.value.reset();
			else
			{
				size_t numConverted = 0;
				gain.value = std::stoi(s, &numConverted);

				if (numConverted != s.size())
					in.setstate(std::ios_base::failbit);
			}
			return in;
		}

		std::string toString(const RTLSDRGain & gain)
		{
			std::stringstream s;
			s << gain;
			return s.str();
		}

		// this works OK with my RTL SDR tuner
		constexpr RTLSDRGain RTLSDR_DEFAULT_GAIN = { -99 };

		po::options_description getRTLSDRBackendOptions()
		{
			po::options_description gpiodBackendOptions("RTLSDR Backend options");
			gpiodBackendOptions.add_options()
				((DEVICE_IDX + ",i").c_str(), po::value<uint32_t>(),
					("RTL SDR device index. default: "s + std::to_string(RTLSDR_DEFAULT_DEVICE_IDX)).c_str())
				(BUFFER_SIZE.c_str(), po::value<size_t>(),
					("Buffer size. Default: "s + std::to_string(RTLSDR_DEFAULT_BUFFER_SIZE)).c_str())
				(BUFFER_COUNT.c_str(), po::value<size_t>(),
					("Buffer count. Default: "s + std::to_string(RTLSDR_DEFAULT_BUFFER_COUNT)).c_str())
				(GAIN.c_str(), po::value<RTLSDRGain>(),
					("Gain. A number or 'auto'. Default: "s + toString(RTLSDR_DEFAULT_GAIN)).c_str())
				(LOG_NAME.c_str(), po::value<std::string>(),
					"Debug log file with captured RTL SDR IQ data.")
			;

			return gpiodBackendOptions;
		}

		void storeRTLSDRBackendOptions(ReceiveBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onRTLSDRBackendParams(
				valueOr(variablesMap, DEVICE_IDX, RTLSDR_DEFAULT_DEVICE_IDX),
				valueOr(variablesMap, BUFFER_SIZE, RTLSDR_DEFAULT_BUFFER_SIZE),
				valueOr(variablesMap, BUFFER_COUNT, RTLSDR_DEFAULT_BUFFER_COUNT),
				valueOr(variablesMap, GAIN, RTLSDR_DEFAULT_GAIN).value,
				valueOr(variablesMap, LOG_NAME, std::string())
			);
		}
#endif

		BackendDescriptions<ReceiveBackendParametersReceiver> makeBackendDescriptions()
		{
			BackendDescriptions<ReceiveBackendParametersReceiver> backendOptions;

			backendOptions.emplace(BACKEND_RPI_GPIO, std::pair(getRPiGPIOBackendOptions(), &storeRPiGPIOBackendOptions));
#ifdef RTS_HAVE_GPIOD
			backendOptions.emplace(BACKEND_GPIOD, std::pair(getGPIODBackendOptions(), &storeGPIODBackendOptions));
#endif
#ifdef RTS_HAVE_RTLSDR
			backendOptions.emplace("rtlsdr", std::pair(getRTLSDRBackendOptions(), &storeRTLSDRBackendOptions));
#endif

			return backendOptions;
		}
	}

	namespace gpio_receiver
	{
		po::options_description getRPiGPIOBackendOptions()
		{
			// the options are the same as for the RPi GPIO frame backend
			return frame_receiver::getRPiGPIOBackendOptions();
		}

		void storeRPiGPIOBackendOptions(GPIOReceiveBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onRPiGPIOBackendParams(
					variablesMap[GPIO_NR].as<unsigned>(),
					valueOr(variablesMap, BUFFER_SIZE, RPI_GPIO_DEFAULT_BUFFER_SIZE),
					std::chrono::microseconds(valueOr(variablesMap, SAMPLE_PERIOD, RPI_GPIO_DEFAULT_SAMPLE_PERIOD_US))
			);
		}

#ifdef RTS_HAVE_GPIOD
		po::options_description getGPIODBackendOptions()
		{
			// the options are the same as for the GPIOD frame backend
			return frame_receiver::getGPIODBackendOptions();
		}

		void storeGPIODBackendOptions(GPIOReceiveBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onGPIODBackendParams(
				variablesMap[CHIP].as<std::filesystem::path>(),
				variablesMap[LINE].as<unsigned>(),
				variablesMap.count(ACTIVE_LOW) != 0,
				valueOr(variablesMap, BUFFER_SIZE, GPIOD_DEFAULT_BUFFER_SIZE)
			);
		}
#endif

		BackendDescriptions<GPIOReceiveBackendParametersReceiver> makeBackendDescriptions()
		{
			BackendDescriptions<GPIOReceiveBackendParametersReceiver> backendOptions;

			backendOptions.emplace(BACKEND_RPI_GPIO, std::pair(getRPiGPIOBackendOptions(), &storeRPiGPIOBackendOptions));
#ifdef RTS_HAVE_GPIOD
			backendOptions.emplace(BACKEND_GPIOD, std::pair(getGPIODBackendOptions(), &storeGPIODBackendOptions));
#endif

			return backendOptions;
		}
	}
}

std::optional<po::variables_map> parseReceiveOptions(int argc, char * argv[],
		const po::options_description & toolOptions,
		ReceiveBackendParametersReceiver & paramsReceiver)
{
	return parseDependentOptions(argc, argv, toolOptions, frame_receiver::makeBackendDescriptions(), paramsReceiver);
}

std::optional<boost::program_options::variables_map> parseGPIOReceiveOptions(int argc, char * argv[],
		const boost::program_options::options_description & toolOptions, GPIOReceiveBackendParametersReceiver & paramsReceiver)
{
	return parseDependentOptions(argc, argv, toolOptions, gpio_receiver::makeBackendDescriptions(), paramsReceiver);
}
