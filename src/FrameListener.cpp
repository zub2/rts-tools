/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FrameListener.h"

#include <iostream>
#include <iomanip>
#include <sstream>

void FrameListener::onFrameDetected(rts::SomfyRemoteType remoteType, rts::SomfyFrameType frameType)
{
	std::cout << ">>> GOT SOMFY FRAME [remoteType=";
	switch (remoteType)
	{
	case rts::SomfyRemoteType::telis:
		std::cout << "Telis";
		break;
	case rts::SomfyRemoteType::keytis:
		std::cout << "Keytis";
	}

	std::cout << ", frameType=";
	switch (frameType)
	{
	case rts::SomfyFrameType::normal:
		std::cout << "normal";
		break;
	case rts::SomfyFrameType::repeat:
		std::cout << "repeat";
		break;
	}
	std::cout << "] <<<" << std::endl;
	std::cout.flush();
}

void FrameListener::onFrameDecoded(rts::SomfyFrameType frameType, const rts::SomfyTelisFrame & frame)
{
	logFrame(frame);
}

void FrameListener::onFrameDecoded(rts::SomfyFrameType frameType, const rts::SomfyKeytisFrame & frame)
{
	logFrame(frame);

	// log the extra bytes
	logByteArray("extra data", frame.getExtraBytes());
}

void FrameListener::onFrameDecodeError(FrameError error, const std::vector<bool> & bits)
{
	std::cout << "decoding failed: ";
	switch (error)
	{
	case FrameError::DecodingError:
		std::cout << " error while decoding bits\n";
		std::cout << "decoding failed after " << bits.size() << " bits\n";
		break;
	case FrameError::WrongChecksum:
		std::cout << " wrong frame checksum\n";
		break;
	}

	std::cout << "decoded bits: " << stringifyBits(bits) << std::endl;
}

void FrameListener::logFrame(const rts::SomfyFrameBase & frame)
{
	std::cout << std::hex << std::setfill('0');
	std::cout << "key: 0x" << std::setw(2) << static_cast<uint16_t>(frame.getKey()) << '\n';
	std::cout << "code: 0x" << static_cast<uint16_t>(frame.getCtrl()) << " [" << getButtonName(frame.getCtrl()) << "]\n";
	std::cout << "rolling code: 0x" << std::setw(4) << frame.getRollingCode() << '\n';
	std::cout << "address: 0x" << std::setw(6) << frame.getAddress() << std::endl;
}

template<std::size_t SIZE>
void FrameListener::logByteArray(const std::string & description, const std::array<uint8_t, SIZE> & bytes)
{
	// log bits first
	std::cout << description << " (bits): ";
	logBits(bytes);

	// log bytes
	std::cout << '\n' << description  << " (bytes): ";
	logBytes(bytes);
	std::cout << '\n';
}

template<std::size_t SIZE>
void FrameListener::logBits(const std::array<uint8_t, SIZE> & bytes)
{
	for (std::size_t i = 0; i < SIZE; i++)
	{
		if (i != 0)
			std::cout << '|';

		const uint8_t byte = bytes[i];
		for (unsigned bitIdx = 0; bitIdx < CHAR_BIT; bitIdx++)
		{
			if (bitIdx == 4)
				std::cout << '.';

			std::cout << ((byte >> (CHAR_BIT - 1 - bitIdx)) & 1);
		}
	}
}

template<std::size_t SIZE>
void FrameListener::logBytes(const std::array<uint8_t, SIZE> & bytes)
{
	std::cout << std::hex << std::setfill('0');
	for (std::size_t i = 0; i < SIZE; i++)
	{
		if (i != 0)
			std::cout << ' ';

		std::cout << "0x" << std::setw(2) << static_cast<unsigned>(bytes[i]);
	}
}

std::string FrameListener::getButtonName(rts::SomfyTelisFrame::Action code)
{
	using Action = rts::SomfyTelisFrame::Action;

	switch (code)
	{
	case Action::my:
		return "My";

	case Action::up:
		return "Up";

	case Action::my_up:
		return "My + Up";

	case Action::down:
		return "Down";

	case Action::my_down:
		return "My + Down";

	case Action::up_down:
		return "Up + Down";

	case Action::prog:
		return "Prog";

	case Action::sun_flag:
		return "Sun + Flag";

	case Action::flag:
		return "Flag";

	default:
		return "unknown";
	}
}

std::string FrameListener::stringifyBits(const std::vector<bool> & bits)
{
	std::stringstream s;

	for (size_t i = 0; i < bits.size(); i++)
	{
		if (i != 0 && i % CHAR_BIT == 0)
			s << '|';
		else if (i != 0 && i % (CHAR_BIT/2) == 0)
			s << '.';
		s << "01"[bits[i]];
	}
	return s.str();
}
