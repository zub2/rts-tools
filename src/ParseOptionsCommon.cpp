/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ParseOptionsCommon.h"

const std::string BACKEND_RPI_GPIO = "rpi-gpio";
const std::string RPI_GPIO_DESCRIPTION_NAME = "RPi GPIO Backend options";

const std::string BACKEND_GPIOD = "gpiod";
const std::string GPIOD_DESCRIPTION_NAME = "GPIOD Backend options";

const std::string BACKEND_ESP8266_RTS = "esp8266-rts";
const std::string ESP8266_RTS_DESCRIPTION_NAME = "ESP8266 RTS Backend options";

const std::string BACKEND_DEBUG = "debug";
const std::string DEBUG_DESCRIPTION_NAME = "Debug Backend options";

const std::string GPIO_NR = "gpio-nr";

#ifdef RTS_HAVE_GPIOD
const std::string CHIP = "chip";
const std::string LINE = "line";
const std::string ACTIVE_LOW = "active-low";
#endif
