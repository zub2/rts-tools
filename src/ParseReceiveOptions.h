/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARSE_RECEIVE_OPTIONS_H
#define PARSE_RECEIVE_OPTIONS_H

#include <optional>
#include <string>
#include <filesystem>

#include <boost/program_options.hpp>

#include "rts/Clock.h"

class ReceiveBackendParametersReceiver
{
public:
	virtual void onRPiGPIOBackendParams(unsigned gpioNr, size_t bufferSize, const rts::Clock::duration & samplePeriod) = 0;
#ifdef RTS_HAVE_GPIOD
	virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned offset, bool activeLow, size_t bufferSize) = 0;
#endif
#ifdef RTS_HAVE_RTLSDR
	virtual void onRTLSDRBackendParams(uint32_t deviceIndex, size_t bufferSize,
			size_t bufferCount, const std::optional<int> & gain, const std::string & logName) = 0;
#endif

protected:
	virtual ~ReceiveBackendParametersReceiver() = default;
};

std::optional<boost::program_options::variables_map> parseReceiveOptions(int argc, char * argv[],
		const boost::program_options::options_description & toolOptions, ReceiveBackendParametersReceiver & paramsReceiver);

class GPIOReceiveBackendParametersReceiver
{
public:
	virtual void onRPiGPIOBackendParams(unsigned gpioNr, size_t bufferSize, const rts::Clock::duration & samplePeriod) = 0;
#ifdef RTS_HAVE_GPIOD
	virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned offset, bool activeLow, size_t bufferSize) = 0;
#endif

protected:
	virtual ~GPIOReceiveBackendParametersReceiver() = default;
};

std::optional<boost::program_options::variables_map> parseGPIOReceiveOptions(int argc, char * argv[],
		const boost::program_options::options_description & toolOptions, GPIOReceiveBackendParametersReceiver & paramsReceiver);

#endif // PARSE_RECEIVE_OPTIONS_H
