# Sending RTS frame with a Raspberry Pi and MX-FS-03V

This is the easiest setup for broadcasting RTS frames with rts-tool.

## Hardware setup

You will need:

* a Raspberry Pi (tested with Raspberry Pi 1 and 3)
* [MX-FS-03V](https://hobbycomponents.com/wired-wireless/617-433mhz-wireless-transmitter-module-mx-fs-03v) (there are many sellers, but this link includes a schematic)
* three wires to connect the transmitter to the Raspberry Pi (Depending on whether you use a breadbord or connect the module directly, you can get [female/male jumper wires](https://www.adafruit.com/product/824) or [female/female jumper wires](https://www.adafruit.com/product/266). Both the Raspberry Pi headers and the MX-FS-03V connection use the common 2.45mm DuPont connectors.)
* an antenna as the MX-FS-03V does not come with one (a piece of wire of the correct lenght will do)

Some minimal soldering is needed to connect the antenna. No other electrical components are needed, but a breadbord is useful because it can work as a stand for the transmitter module.

### Connecting the antenna

The easiest option is just a piece of straight wire - a quarter wave monopole antenna. As the frequency is 433 MHz, the wave length is c/433 MHz, so approx. 69.2 cm. A quarter of that is 17.3 cm. So you will need a straight piece of wire with length of a bit over 17.3 cm (soldering it into the PCB needs a millimeter or two - although the precision here is not so critical anyway).

The antenna spot is somewhat labeled on the transmitter PCB, but it's not all that clear. When looking at the module from the front side (where the pin labels are and where the larger components like the coils and the SAW resonator are placed), the antenna connection is the hole in the upper right corner.

### Connecting the module to the Raspberry Pi

Warning! Connecting the transmitter to the wrong pins or accidentally shorting some wires while fiddling with the transmitter could kill your Raspberyr Pi. Always connect and disconnect the wires when the Raspberry Pi is disconnected from power supply and double check that you connected the right pins.

The MX-FS-03V's three pins need to be connected as follows:

* GND: the ground on the RPi
* VCC: to the positive supply voltage
* DATA: to a GPIO pin that will then be controlled by the transmitting software

(The labeling of the pins is a bit confusing. But indeed, the VCC is the middle pin, and the DATA pin is the leftmost pin when looking at the front side of the module.).

The connections need to be made to the P1 header on the Raspberry Pi. These differ between RPi 1 and RPi 2 & 3. A good summary including photos is available on [elinux.org](https://elinux.org/RPi_Low-level_peripherals#General_Purpose_Input.2FOutput_.28GPIO.29). Luckily, the first 26 pins have the same functionality in all RPi versions (the RPi 2 and 3 just added some extra pins).

The RPi GPIO pins run at 3.3V. The MX-FS-03V operating range is 3.5V - 12V. While it seems to run at 3.3V too, the RPi has a 5V power rail on the P1 connector. Looking at the schematic of the MX-FS-03V (recreated from the image at [hobbycomponents.com](https://hobbycomponents.com/wired-wireless/617-433mhz-wireless-transmitter-module-mx-fs-03v)), it can be seen that the data pin is connected to a base of a bipolar transistor (via a 3.9kΩ resistor):

![MX-FS-03V schematic](images/MX-FS-03V schematic.svg)

So it's possible to connect the VCC of the transmitter to the +5V rail, while connecting the DATA pin directly to a GPIO.

While any GPIO pin could be used, beware that the GPIO pins have also other functionality and can be configured e.g. for SPI, or UART. (see the [explanation at elinux.org](https://elinux.org/RPi_Low-level_peripherals#Interfacing_with_GPIO_pins)). As we need the ground and the +5V, a close and "safe" candidate is pin 7 (GPIO4), so let's connect the pins as follows:

| MX-FS-03V | Header P1 on Raspberry Pi 1, 2 or 3 |
| --------- | -------------------------------------- |
| GND       | pin 6 (GND)                            |
| VCC       | pin 2 (+5V)                            |
| DATA      | pin 7 (GPIO4)                          |

![MX-FS-03V connected to the Raspberry Pi 1](images/RPi1-MX-FS-03V-setup.jpg)

## Software setup

While the rts-tools have several backends, for the RPi you want to use the `rpi-gpio` backend. (The `gpiod` backend is way too slow.)

Currently you first need to "reserve" and configure the GPIO line. For this the old sysfs GPIO kernel support is still needed. As root, run the following commands:

```shell
echo 4 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio4/direction

```

Then you should be able to transmit an actual RTS frame running a command like the following (again as root):

```shell
./somfy-transmitter -B rpi-gpio -n 4 -a 0x200000 -c down -k 0xa4 -r 8
```

where:

* `-B rpi-gpio` selects the rpi-gpio backend
* `-n 4` selects the GPIO pin to use (GPIO 4)
* `-a 0x200000` is the address to put in the frame
* `-c down` is the command
* `-k 0xa4` is the key
* `-r 8` is the rolling code
