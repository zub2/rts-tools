# rts-tools

A library and a set of tools to receive, decode, and transmit Somfy RTS Protocol frames.

The protocol is used by some blinds, shades, rolling shutters, awnings, etc. by [Somfy](https://www.somfysystems.com/). If you have such a device, you can use this software to both receive what the Somfy remote sends and also to broadcast your own commands.

Two remotes are supported:
* Telis - the code is based on the description available at [Pushstack blog](https://pushstack.wordpress.com/somfy-rts-protocol/). Telis frames can be received and transmitted.
* Keytis - only partially supported: frames can be recived but extra payload is not decoded and Keytis frames can't be broadcast. The code is based on logs provided by Mariusz Białończyk.

## Compiling

You will need:

* [meson](http://mesonbuild.com/)
* a C++ compiler supporting C++ 17 (e.g. gcc 8)
* [boost](http://boost.org/) libraries
* [libgpiodcxx v2.x](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/) (C++ binding for libgiod) - this is optional but highly recommended; without this dependency the user has to configure GPIOs (direction etc.) themselves even for the RPi GPIO backend (for v1.x see the git branch `gpiod-v1.x`)

The rtl_sdr backend is optional and requires [librtlsdr](https://osmocom.org/projects/sdr/wiki/rtl-sdr).

## Hardware

A RF hardware is needed to do any actual receiving or transmiting. _You don't say?_ :) Currently the following devices are supported:

* Any RF receiver supported by [rtl_sdr](https://osmocom.org/projects/sdr/wiki/rtl-sdr). This is quite easy to get running as you can just connect a USB tuner to a PC. The device needs to allow tuning the RTS frequency - 433.42 MHz. Not surprisingly, rtl_sdr devices can only be used for receiving RTS frames. A RTL2838 DVB-T USB dongle works OK for me.
* Any OOK receiver or transmitter connected via GPIO.
	* There is a special GPIO implementation for the Raspberry Pi. This is faster than the Linux GPIO interface although this should make no difference for receiving.
	* An implementation using Linux GPIO interface. This should work on any target with non-ancient kernel. But the latency might be too high and result in the timing so off that the RTS frames are not recognized.

### Hardware Setup

#### rtl-sdr

rtl-sdr support is implemented by the "rtlsdr" backend. Using it should be straightforward. Just make sure you have librtlsdr headers when you compile rts-tools.

#### OOK transmitter or receiver connected via GPIO

This is more complex to set up, but this can be used to both receive or transmit. There is a dedicated support for GPIO on the Raspberry Pi, but there is also support for [the (relatively new) /dev/gpiochip Linux GPIO interface](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/uapi/linux/gpio.h?h=v5.0). Regardless of which backend you use, the hardware setup is the same.

rts-tools either poll on, or write to, a GPIO line. As the RTS protocol uses OOK modulation, you need an OOK transmitter and/or an OOK receiver module. The ubiquitous and cheap [MX-FS-03V and MX-05](http://hobbycomponents.com/wired-wireless/168-433mhz-wireless-modules-mx-fs-03v-mx-05) worked OK enough for me, though they are not ideal as the RTS devices use 433.42 MHz while the modules use 433.92 MHz. There are some other options discussed in the comments at the [PushStack blog](https://pushstack.wordpress.com/somfy-rts-protocol/).

One has to pay attention to the voltage and current the GPIOs of your board can handle. E.g. for the Raspberry Pi, the GPIOs run at 3.3V and you must not connect them to 5V. This gets even trickier with the Espressobin which has (by default) 1.8V GPIOs.

## Tools

This is essentially a C++ version of [octave-somfy](https://gitlab.com/zub2/octave-somfy) extended by the ability to record and transmit.

The following tools are included:

* low-level tools for recording/controlling GPIOs - this can be used for debugging
	* gpio-logger
	* gpio-transmitter
* RTS decoding and transmitting programs
	* somfy-decoder
	* somfy-transmitter
* offline decoding from logs
	* gpio-log-somfy-decoder
	* sdr-log-somfy-decoder

When using the programs, note that each supports option `-h` (or `--help`) that prints some basic description of the arguments it accepts.

In order to decrease probability of dropping data or transmitting broken frames when using GPIO backends, realtime priority ([SCHED_FIFO](http://linuxrealtime.org/index.php/Basic_Linux_from_a_Real-Time_Perspective#SCHED_FIFO_and_SCHED_RR)) threads are used. This requires root privileges. The high-priority threads only deal with sampling (if needed) and writing GPIO data.

Reading using the RPi GPIO backend requires frequent polling, so it's more CPU intensive. While it should not eat up the whole CPU and leave the Raspberry Pi frozen, be warned that if some bug causes the reading or writing thread to misbehave, the Raspberry Pi could become non-responsive.

The RPi GPIO backend also uses direct memory access, which is another reason why it needs root privileges.

### gpio-logger

A tool that samples a GPIO input and records durations between transitions. The resulting file can be loaded by [octave-somfy](https://gitlab.com/zub2/octave-somfy) via the function `loadAndDecodeGPIOLog`.

To decrease jitter the tool works by preallocating a buffer of configurable size and then it keeps sampling the GPIO using a selected backend using a thread with realtime priority. Only timepoints of transitions are stored in the log, so the size of buffer needed depends on how many transitions there are. The recording stops either after a given number of seconds (-d) passes or when the log becomes full.

Example:

```
# gpio-logger -B gpiod -C /dev/gpiochip0 -L 10 -f test_log.bin
Starting recording... done
```

### gpio-transmitter

A tool that can play back a text description of durations and values through a GPIO output.

The format of the description file is simple: Each line must be either empty or contain a number specifying the duration in µs, one or more whitespace characters and the output value (0, 1, or the strings false and true). Leading and trailing whitespace is ignored. The character `#` begins a comment that ends at the end of the line.

There is an example file (in `examples/gpio-play-test.log`) that can be played back:

```
# gpio-transmitter -B gpiod -C /dev/gpiochip0 -L 10 -f examples/gpio-play-test.log
```

If you connect a LED with a resistor to the output, you should see it blink.

### somfy-decoder

A tool that can decode Somfy RTS frames from any backend supported by librts.

Example:

```
# somfy-decoder -B rpi-gpio -n 10
>>> GOT SOMFY FRAME [type=normal] <<<
got all bits!
decoded bits: 1010.0001|1110.0111|1110.1110|1000.0110|1011.0101|1101.1100|1001.1001
decoded bytes: a1|e7|ee|86|b5|dc|99
key: 0xa1
code: 0x4 [Down]
rolling code: 0x0968
address: 0x336945
>>> GOT SOMFY FRAME [type=repeat] <<<
got all bits!
decoded bits: 1010.0001|1110.0111|1110.1110|1000.0110|1011.0101|1101.1100|1001.1001
decoded bytes: a1|e7|ee|86|b5|dc|99
key: 0xa1
code: 0x4 [Down]
rolling code: 0x0968
address: 0x336945
```

Although sometimes the decoding fails, e.g.:

```
>>> GOT SOMFY FRAME [type=repeat] <<<
DECODER error: expecting short transition
decoding failed after 13 bits!
decoded bits: 1010.0001|1110.0111|110
```

### somfy-transmitter

A tool that can transmit Somfy RTS frames via any backend supported by librts. (Well, currently that means an OOK receiver connected to GPIO.)

As the RTS device keeps a list of associated remotes and a rolling counter for each, please first read up on the Somfy RTS protocol at the [PushStack blog](https://pushstack.wordpress.com/somfy-rts-protocol/).

If you want to control your blinds/shutters/what not from the Raspberry Pi or other device, you probably want to create a new "virtual" remote, rather than interfere with your existing one.

An example invocation looks like this:

```
# somfy-transmitter -B rpi-gpio -n 17 -k 0xa4 -c down -r 335 -a 0x100001
```

### gpio-log-somfy-decoder

A tool that can decode RTS frame from a log created by gpio-logger.

Example:

```
$ gpio-log-somfy-decoder -f gpio-log.bin
```

TODO: provide an actual example

### sdr-log-somfy-decoder

A tool that can decode RTS frame from a rtl-sdr IQ log. A suitable log can be captured by a command like `rtl_sdr -f 433420000 -s 2600000 -g 1 captured_data.iq`.

Example:

```
$ sdr-log-somfy-decoder -f captured_data.iq
>>> GOT SOMFY FRAME [type=normal] <<<
key: 0xa6
code: 0x4 [Down]
rolling code: 0x0ba6
address: 0x534392
>>> GOT SOMFY FRAME [type=repeat] <<<
key: 0xa6
code: 0x4 [Down]
rolling code: 0x0ba6
address: 0x534392
```

## Tests

There are also some tests in the directory `subprojects/librts/test`. They can be run either via the build system (e.g. `ninja test`) or by running the executable `tests` directly.

## librts

The reusable parts are accessible via the library librts. The original code was written in a way that allows the compiler to inline and optimize it. E.g. Templates were used in place of virtual methods. I did not want to lose this, so still a lot of the code is in headers and it will end up inlined in the application. But the point of librts is in allowing reuse of the code, not so much in sharing the code. That's why only static library is built by default.

The library contains two layers:

* low-lewel: this allows access to pretty much all classes, in the same way as they have been used in rts-tools
* high-level interface: this adds an interface on top of the low-level interface. The virtual calls are added on top, so the internals can still be inlined and optimized. This is used in somfy-receiver and somfy-transmitter.

## Links

This is not the only piece of software that can be used for the task. But finding other useful projects is, in my opinion, difficult because they are mostly obscure. Anyway here are some links I've found:

* [NodeMCU](https://en.wikipedia.org/wiki/NodeMCU)'s [Somfy module](https://nodemcu.readthedocs.io/en/master/en/modules/somfy/) - It seems the NodeMCU (an [ESP8266](https://en.wikipedia.org/wiki/ESP8266) with [custom firmware](http://nodemcu.com/index_en.html)) contains a module that can send Somfy RTS commands via a GPIO-connected OOK transmitter. I found this via [NodeMCU-Somfy](https://github.com/StryKaizer/NodeMCU-Somfy) which adds a web interface on top of it.
* [Somfy-Remote](https://github.com/Nickduino/Somfy_Remote) - An Arduino Sketch that can send a Somfy RTS frame via a GPIO-connected OOK transmitter. (NodeMCU's somfy module mentions it's based on this)
* [OpenHAB](http://www.openhab.org/) has binding to [RFXCOM devices](https://docs.openhab.org/addons/bindings/rfxcom1/readme.html). With the
[RFXtrx433E](http://www.rfxcom.com/RFXtrx433E-USB-43392MHz-Transceiver/en) and the binding you can send Somfy RTS frames.
* [henrythasler/sdr](https://github.com/henrythasler/sdr) - A collection of tools related to SDR, [Somfy RTS receiver and transmitter](https://github.com/henrythasler/sdr/tree/master/somfy) are included. Also contains some useful RF intro documents.

There are also some interesting devices:

* [RFXtrx433E](http://www.rfxcom.com/RFXtrx433E-USB-43392MHz-Transceiver/en) which I discovered via the [OpenHAB binding](https://docs.openhab.org/addons/bindings/rfxcom1/readme.html). It connects via USB, so you can connect it to any PC and you don't have to worry about timing, the device handles that. It also has a transmitter at the correct frequency for Somfy RTS (433.42 MHz). It presents itself as a serial device so communicating with it is also easy. But it's not cheap and as far as I can tell it can't receive Somfy RTS frames.
* [RFM69](https://www.hoperf.com/%20modules/rf_transceiver/RFM69HW.html) (used by [henrythasler/sdr Somfy RTS receiver and transmitter](https://github.com/henrythasler/sdr/tree/master/somfy)) - It connects via [SPI](https://cs.wikipedia.org/wiki/Serial_Peripheral_Interface) and seems to have [many features](http://www.hoperf.com/upload/rf/RFM69HW-V1.3.pdf).
